#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Pokemon.h"
using namespace std;

void splashScreen()
{
	cout << R"( ____   ___   __  _    ___  ___ ___   ___   ____                 
|    \ /   \ |  |/ ]  /  _]|   |   | /   \ |    \                
|  o  )     ||  ' /  /  [_ | _   _ ||     ||  _  |               
|   _/|  O  ||    \ |    _]|  \_/  ||  O  ||  |  |               
|  |  |     ||     ||   [_ |   |   ||     ||  |  |               
|  |  |     ||  .  ||     ||   |   ||     ||  |  |               
|__|   \___/ |__|\_||_____||___|___| \___/ |__|__|               
                                                                 
    _____ ____  ___ ___  __ __  _       ____  ______   ___   ____  
   / ___/|    ||   |   ||  |  || |     /    ||      | /   \ |    \ 
  (   \_  |  | | _   _ ||  |  || |    |  o  ||      ||     ||  D  )
   \__  | |  | |  \_/  ||  |  || |___ |     ||_|  |_||  O  ||    / 
   /  \ | |  | |   |   ||  :  ||     ||  _  |  |  |  |     ||    \ 
   \    | |  | |   |   ||     ||     ||  |  |  |  |  |     ||  .  \
    \___||____||___|___| \__,_||_____||__|__|  |__|   \___/ |__|\_| 
                                          By Michael Joshua A. Garay of TG102 )" << endl;
}

void main()
{
	srand(time(NULL));
	int randDamage = rand() % 20 + 10;
	int randxP = rand() % 20 + 1;
	int randlvl = rand() % 10 + 1;
	int enc = rand() % 15 + 1;
	int cap = rand() % 10 + 1;
	Player* Trainer = new Player("");
	Pokemons* wild = new Pokemons(100, 0, randDamage, 0, 40, randlvl);
	splashScreen();
	system("pause");
	system("cls");
	Trainer->introduction("");
	Trainer->move(0,0,1, NULL);

}